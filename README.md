# theories/maths-top

## About this project

If you want to add new mathematical hypothesis to prove or need help to found any expression result, this is the right place.

Add new **issue** with label *hypothesis* (title formated as **name of request [hypothesis]**) or label *computation* (**name of request [computation]**). The maintainer will create a separated repository for your request.

You can find the complete list of projects in group **maths** [here](https://gitlab.com/theories/maths).

## Contributing

See [CONTRIBUTING.md](CONTRIBUTING.md) file for more details.

## Licenses

Any project content in this project is under [**Creative Commons LICENSE**](LICENSE.txt) and any project code content in this project is under [**MIT LICENSE**](LICENSE-CODE.txt).